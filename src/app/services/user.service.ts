import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';

@Injectable()
export class UserService {
  size = 8;
  private userUrl = '';
  constructor(private http: HttpClient) {
  }

  getUsers(): Observable<any> {
    console.log(this.userUrl);
    return this.http.get('https://randomuser.me/api/?inc=gender,name,picture,location&results=' + this.size + '&nat=gb');
  }

  setSize(size) {
    this.size = size;
  }
}
